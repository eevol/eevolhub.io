---
title: babysitting.lu
subtitle: find a babysitter in luxembourg
layout: default
modal-id: 3
date: 2016-06-01
img: babysitting.png
thumbnail: babysitting_thumbnail.png
alt: image-alt
project-date: since September 2015
client: Service National de la Jeunesse
category: Web Development
description: eevol developed and maintains this custom web application that brings together parents and babysitters in luxembourg. The main goal for the first version was to replace the old drupal based system with a system that is easier to maintain and easier to enrich with new features. During the project, eevol developed a styleguide, migrated the data from the old system and took care of all aspects of the project management. The the new django based system has been released in June 2016 and eevol is proud that we could already deliver more functionality and a much faster system than it was before.
---
